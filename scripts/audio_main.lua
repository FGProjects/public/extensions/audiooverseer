-- 
-- Please see the license.html file included with this distribution for 
-- attribution and copyright information.
--

function onInit()
	update();
end

function updateControl(sControl, bReadOnly, bForceHide)
	if not self[sControl] then
		return false;
	end
		
	return self[sControl].update(bReadOnly, bForceHide);
end

function update()
	local nodeRecord = getDatabaseNode();
	local bReadOnly = WindowManager.getReadOnlyState(nodeRecord);

	updateControl("category", bReadOnly);
	updateControl("type", bReadOnly);
	updateControl("subtype", bReadOnly);
	updateControl("pack", bReadOnly);
	updateControl("set", bReadOnly);
	updateControl("path", bReadOnly);

  if DB.getValue(nodeRecord,"path_stop","") == "" and bReadOnly then
    updateControl("path_stop", bReadOnly,true);
  else
    updateControl("path_stop", bReadOnly);
  end
  
  if DB.getValue(nodeRecord,"path2","") == "" and bReadOnly then
    updateControl("path2", bReadOnly,true);
  else
    updateControl("path2", bReadOnly);
  end
  
  if DB.getValue(nodeRecord,"path2_stop","") == "" and bReadOnly then
    updateControl("path2_stop", bReadOnly,true);
  else
    updateControl("path2_stop", bReadOnly);
  end
end
