-- 
-- Please see the license.html file included with this distribution for 
-- attribution and copyright information.
--

MIN_LAST_TIME_PLAYED=2.0;
aSoundsList = {};

OOB_AUDIOTRIGGER = "audiotrigger";
function onInit()
  ChatManager.registerReceiveMessageCallback(onReceiveMessageTrigger);

  OOBManager.registerOOBMsgHandler(OOB_AUDIOTRIGGER, handleAudioTrigger);
  --
  OptionsManager.registerOption2("AUDIOOVERSEER", false, "option_header_client", "option_label_AudioOverseer", "option_entry_cycler",
  { labels = "disabled|notriggers", values = "disabled|notriggers", baselabel = "enabled", baseval = "enabled", default = "enabled" });
  --
  OptionsManager.registerOption2("AUDIOOVERSEER_SOURCE", false, "option_header_client", "option_label_AudioOverseer_Source", "option_entry_cycler",
  { labels = "path2", values = "path2", baselabel = "path1", baseval = "path1", default = "path1" });
  if User.isHost() then
    Comm.registerSlashHandler("ao", processAudioOverseerSlash);
    Comm.registerSlashHandler("audiooverseer", processAudioOverseerSlash);
  end
end

-- store the url token
function processAudioOverseerSlash(sCommand,sParams)
  local sCmd, sCmdParameter = sParams:match("^([%w]+) (.+)");
  if not sCmd then sCmd = sParams; end;
  if sCmd == "token" and sCmdParameter then
    urlTokenStore(sCmdParameter);
  elseif sCmd == "deletetoken" then
    urlTokenDelete();
  elseif sCmd == "showtoken" then
    urlTokenShow();
  elseif sCmd == "debug" and sCmdParameter then
    urlDebug(sCmdParameter);
  elseif sCmd == "method" and sCmdParameter then
    urlMethod(sCmdParameter);
  else
    showCommandHelp();
  end
end

-- turn debug logging on or off
function urlDebug(sOption)
  local nodeURL = DB.createNode("audiooverseer");
  sOption = sOption:lower();
  local nDebug = 0;
  if sOption:find("on") or sOption:find("enable") then
    ChatManager.SystemMessage("*** AudioOverseer: Debug console mode enabled.");
    nDebug = 1;
  elseif sOption:find("off") or sOption:find("disable") then
    ChatManager.SystemMessage("*** AudioOverseer: Debug console mode disabled.");
  else
    ChatManager.SystemMessage("*** AudioOverseer: Debug options 'on' or 'off'.");
    return;
  end
  DB.setValue(nodeURL,"debug","number",nDebug);
end

-- set URL method type.
function urlMethod(sOption)
  local nodeURL = DB.createNode("audiooverseer");
  sOption = sOption:lower();
  local sURLMethod = "";
  local nDebug = 0;
  if sOption:find("local") then
    ChatManager.SystemMessage("*** AudioOverseer: URL execution method set to local (typically runs through browser).");
    sURLMethod = "local";
  elseif sOption:find("default") then
    ChatManager.SystemMessage("*** AudioOverseer: URL execution method set to default.");
  else
    ChatManager.SystemMessage("*** AudioOverseer: URL method.");
    return;
  end
  DB.setValue(nodeURL,"urlmethod","string",sURLMethod);
end

-- set the TokenURL
function urlTokenStore(sToken)
  local nodeURL = DB.createNode("audiooverseer");
  DB.setValue(nodeURL,"urltoken","string",sToken);
  ChatManager.SystemMessage("*** AudioOverseer: Set url token.");
end
-- clear the tokenURL
function urlTokenDelete()
  local nodeURL = DB.findNode("audiooverseer");
  DB.deleteChild(nodeURL,"urltoken");
  ChatManager.SystemMessage("*** AudioOverseer: Deleted url token.");
end
-- show the TokenURL
function urlTokenShow()
  local nodeURL = DB.createNode("audiooverseer");
  local sToken = DB.getValue(nodeURL,"urltoken","");
  ChatManager.SystemMessage("*** AudioOverseer: Current url token: " .. sToken);
end

function showCommandHelp()
  ChatManager.SystemMessage("AudioOverseer Help\n\r"
                         .. "/ao debug [on|off]               - Console debug mode.\r\n"
                         .. "/ao token [authtoken]        - Store URL token.\r\n"
                         .. "/ao method [default|local] - Method used to execute URL. 'local' typically sends to browser.\r\n"
                         .. "/ao deletetoken                    - Delete URL token.\r\n"
                         .. "/ao showtoken                      - Show current URL token.\r\n"
                         .. "/ao help                                   - Display help.\r\n"
                         .. "");
end

-- wait "s" Seconds for delayed triggers
function sleep(s)
  local ntime = os.clock() + s;
  repeat until os.clock() > ntime;
end

-- return the url method string
function getURLMethod()
  local nodeAO = DB.createNode("audiooverseer");
  local sMethod = DB.getValue(nodeAO,"urlmethod","");
  return sMethod;
end

-- play a path with campaign setting check
function playURL(sURL,nDelay)
  local bEnabled = OptionsManager.isOption("AUDIOOVERSEER","enabled");
  
-- Debug.console("manager_audio.lua","playURL","sURL",sURL);  
-- Debug.console("manager_audio.lua","playURL","nDelay",nDelay);  
-- Debug.console("manager_audio.lua","playURL","bEnabled",bEnabled);
  if bEnabled and sURL and sURL ~= "" then
    if nDelay > 0 then
      sleep(nDelay);
    end

    local cCurrent = os.clock();
    local bToSoon =false;
    local bOverCurrent = false;
    
    -- right now there is only Syrinscape type tokens so this is very specific
    if needsURLToken(sURL) then
      local sToken = getURLToken();
      if sToken then
        -- replace https: with syrinscape: so that syrinscape client sends the sound request
        -- if method set to "local" let the OS send it to a browser via http
        if getURLMethod() ~= "local" then 
          sURL = sURL:gsub("^https","syrinscape-online");
        end
        -- append token
        sURL = sURL .. sToken;
      else
        ChatManager.SystemMessage("*** AudioOverseer: Audio path requires token and could not retrieve one.\r\nType /ao help for how to set.");
        return;
      end
    end    
    -- end token insert 
    
    -- check to see if we're tracking this url
    if aSoundsList[sURL] then
      local cLastPlayed = aSoundsList[sURL];
      -- if it's played already in 1 second we don't play it.
      bToSoon = (cLastPlayed + MIN_LAST_TIME_PLAYED) > cCurrent;
      bOverCurrent = (cCurrent > (cLastPlayed + MIN_LAST_TIME_PLAYED)); 
      -- we've waiting long enough so we clear out the tracker for this url
      if bOverCurrent then
        aSoundsList[sURL] = nil;
      end
    end
    
    if not bToSoon then
      logDebug("manager_audio","playURL","Playing URL:",sURL)      
      Interface.openWindow("url", sURL);
      if not aSoundsList[sURL] or bOverCurrent then
        aSoundsList[sURL]=cCurrent;
      end
    else
      Debug.console("manager_audio.lua","playURL","Receiving same URL to fast, sURL:",sURL);
    end
    
  elseif not bEnabled then
    logDebug("manager_audio","playURL","AudioOverseer sounds are DISABLED in campaign settings.");
  end
end

-- if this returns true need to get a auth token to insert.
function needsURLToken(sURL)
--logDebug("manager_audio.lua","needsURLToken","needsURLToken2",sURL:find("syrinscape%-online"))
  local bSyrinScape = sURL:find("https:\/\/[w]?[w]?[w]?[\.]?syrinscape%.com");
  return bSyrinScape;
end
-- https://www.syrinscape.com/online/frontend-api/elements/9412/play/?auth_token=xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
-- return the URL token
function getURLToken(sURL)
  local nodeURL = DB.createNode("audiooverseer");
  local sToken = DB.getValue(nodeURL,"urltoken");
  if sToken and sToken ~= "" then
    sToken = "?auth_token=" .. sToken;
  end
  return sToken;
end
-- intercept incoming chat messages and run it through filters
function onReceiveMessageTrigger(msg)
  local bTriggersEnabled = not OptionsManager.isOption("AUDIOOVERSEER","notriggers");
  if bTriggersEnabled then 
    local sText = msg.text;
      --  local bSecret = (msg.secret == "bTRUE");
    logDebug("manager_audio","onReceive","Checking line of text ========>",sText);      
    local aMappings = LibraryData.getMappings("audiotriggers");
      -- flip through all triggers
    for _,sMap in ipairs(aMappings) do
      for _,nodeTrigger in pairs(DB.getChildrenGlobal(sMap)) do
        local bEnabledTrigger = (DB.getValue(nodeTrigger,"disabled",0) ~= 1);
        if bEnabledTrigger and matchesFound(sText, nodeTrigger) then

          logDebug("manager_audio","onReceive","Name of Trigger:",DB.getValue(nodeTrigger,"name",""));      

          -- local sModulePath = getModulePath(nodeTrigger.getPath())
          
          logDebug("manager_audio","onReceive","Trigger sound list:",DB.getChildren(nodeTrigger, "soundlist"));

          -- flip through each sound and play it with it's delay
          for _,nodeEntry in pairs(UtilityManager.getSortedTable(DB.getChildren(nodeTrigger, "soundlist"))) do
            local nDelay = DB.getValue(nodeEntry,"delay",0);
            local sSoundRoot = DB.getValue(nodeEntry,"sound");
            logDebug("manager_audio","onReceive","Original Source Sound Record:",sSoundRoot);  
            if (not sSoundRoot:find("@")) then
              sSoundRoot = sSoundRoot .. "@*";  
            end      
            playSoundRecord(DB.findNode(sSoundRoot),false,nDelay)
          end
          -- check for random sounds 
          local sRandomSound = getRandomSound(nodeTrigger);
          if sRandomSound then
            playSoundRecord(DB.findNode(sRandomSound));
          end
        else
          logDebug("manager_audio","onReceive","NO MATCH FOR:",DB.getValue(nodeTrigger,"name",""));
        end
      end
    end
  else
    logDebug("manager_audio","onReceive","AudioOverseer triggers DISABLED in campaign settings.");
  end
end

local textFound = {};
-- flip through all triggers and if all of them found return true
function matchesFound(sText, nodeTrigger)
  -- logDebug("manager_audio.lua","matchesFound","sText",sText);  
  -- logDebug("manager_audio.lua","matchesFound","nodeTrigger",nodeTrigger);  
  local bFoundAll = true;
  -- flip through all matches and check that each match something in this text
  -- for _,nodeMatch in pairs(DB.getChildrenGlobal(nodeTrigger, "matchlist")) do
  for _,nodeMatch in pairs(DB.getChildren(nodeTrigger, "matchlist")) do
    -- logDebug("manager_audio.lua","matchesFound","nodeMatch",nodeMatch);  
    local sMatch = DB.getValue(nodeMatch,"match","");
    -- logDebug("manager_audio.lua","matchesFound","sMatch",sMatch);  
    -- logDebug("manager_audio.lua","matchesFound","sText:match(sMatch)",sText:match(sMatch));  
    if (sMatch == "") or (not sText:match(sMatch)) then
    -- if (not sText:match(sMatch)) then
      bFoundAll = false;
      -- logDebug("manager_audio.lua","matchesFound","matchlist BREAK");  
      -- break;
    elseif (sMatch ~= "") and (sText:match(sMatch)) then
      -- Multi-Line checking support bits
      local foundNodes = textFound[nodeTrigger.getName()] or {};
      if (not StringManager.contains(foundNodes, nodeMatch.getName())) then
        logDebug("manager_audio.lua","matchesFound","found part of multi-line match for nodeTrigger",nodeTrigger.getName(), nodeMatch.getName());
        table.insert(foundNodes,nodeMatch.getName());
        textFound[nodeTrigger.getName()] = foundNodes;      
      else
        -- should we reset textFound for nodeTrigger since we found a piece of it again w/o finding all?
        logDebug("manager_audio.lua","matchesFound","FOUND duplicate match for nodeMatch",nodeMatch.getName());
        logDebug("manager_audio.lua","matchesFound","RESET tracking for nodeTrigger",nodeTrigger.getName());
        foundNodes = {};
        table.insert(foundNodes,nodeMatch.getName());
        logDebug("manager_audio.lua","matchesFound","foundNodes",foundNodes);
      end
      if (#textFound[nodeTrigger.getName()] >= DB.getChildCount(nodeTrigger,'matchlist')) then
        -- we found all the matches for a trigger (even across lines) so mark and be done.
        bFoundAll = true;
        logDebug("manager_audio.lua===============FOUND ALL=============:",DB.getValue(nodeTrigger,"name"));  
        break;
      end
    end
  end
  -- logDebug("manager_audio.lua","matchesFound","DONE!!!");  
  if bFoundAll then
    textFound = {}; -- flush when we find a match
  end
  -- logDebug("manager_audio.lua","matchesFound","bFoundAll", bFoundAll);  
  return bFoundAll;
end

-- if there are randomsounds pick one.
function getRandomSound(nodeTrigger)
  local sSoundRoot = nil;
  -- local aSounds = UtilityManager.getSortedTable(DB.getChildrenGlobal(nodeTrigger, "randomsoundlist"));
  local aSounds = UtilityManager.getSortedTable(DB.getChildren(nodeTrigger, "randomsoundlist"));
  if #aSounds > 0 then
    local nSelected = math.random(1,#aSounds);
    local nodeEntry = aSounds[nSelected];
    sSoundRoot = DB.getValue(nodeEntry,"sound");
    if (not sSoundRoot:find("@")) then
      sSoundRoot = sSoundRoot .. "@*";  
    end      
  end
  logDebug("manager_audio","getRandomSound","sSoundRoot:",sSoundRoot);  
  return sSoundRoot;
end


-- -- get the "module" path if the node is contained in a module.
-- function getModulePath(sNodePath)
--   local _, sModulePath = sNodePath:match("([^@]*)@(.*)");
--   return sModulePath;
-- end

-- play a sound given the record node. audio.id-000001
function playSoundRecord(nodeSound,bStopSound,nDelay)
  local sPath1 = DB.getValue(nodeSound,"path");
  local sPath2 = DB.getValue(nodeSound,"path2");
  local sPath1stop = DB.getValue(nodeSound,"path_stop");
  local sPath2stop = DB.getValue(nodeSound,"path2_stop");
  local sPath = sPath1;
  if bStopSound then
    sPath = sPath1stop;
    sPath2 = sPath2stop;
  end
  if OptionsManager.isOption("AUDIOOVERSEER_SOURCE","path2") then
    sPath = sPath2;
  end
  
  logDebug("manager_audio","playSoundRecord","Sound Record Name",DB.getValue(nodeSound,"name",""));
  
  notifyAudioTrigger(sPath, nDelay);
end

-- notify OOB to take control and handle this node update
function notifyAudioTrigger(sPath, nDelay)
  local msgOOB = {};
  msgOOB.type = OOB_AUDIOTRIGGER;
  
  msgOOB.sPath = sPath;
  msgOOB.nDelay = nDelay;
  
  Comm.deliverOOBMessage(msgOOB, "");
end
-- oob takes control and makes change
function handleAudioTrigger(msgOOB)
  local sPath  = msgOOB.sPath;
  local nDelay = tonumber(msgOOB.nDelay) or 0;
 
  -- play sound
  playURL(sPath,nDelay);
end

-- put timestamp in log output
function logDebug(...)
  local nodeAO = DB.createNode("audiooverseer");
  local bDebug = (DB.getValue(nodeAO,"debug",0) == 1);
  if bDebug then
    Debug.console("[" .. os.date("%x %X") .. "]",...);
  end
end
