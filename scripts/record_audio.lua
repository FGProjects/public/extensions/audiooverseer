-- 
-- Please see the license.html file included with this distribution for 
-- attribution and copyright information.
--

function onInit()
  onValueChanged();
end

-- when a path value is changed, update the url links
function onValueChanged()
  local sSelfName = self.getName()
  local sPath = getValue();
  local ctl = nil;
  local sClass = "urlaudio";
  
  -- set class for urlaudio
  if sSelfName == "path" then
    ctl = window.audioshortcut
  elseif sSelfName == "path2" then
    ctl = window.audioshortcut2
  end

  -- set class for urlaudiostop
  if sSelfName == "path_stop" then
    ctl = window.audiostopshortcut
    sClass = "urlaudiostop";
  elseif sSelfName == "path2_stop" then
    ctl = window.audiostopshortcut2
    sClass = "urlaudiostop";
  end

  if ctl then
    if sPath and sPath ~= "" then
      ctl.setValue(sClass);
    else
      ctl.setValue("");
    end
  end
end
          